# Java Spring REST API

## Live demo
https://hader-product-api.herokuapp.com/product

## Requirements
* _Maven 3_ (https://maven.apache.org/download.cgi) 
* _Java 1.8_ (http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

## To run the application
* git clone https://bitbucket.org/hader-araujo/server-product-api
* cd product-server-api
* mvn clean install
* mvn jetty:run

## To run the automated tests
* mvn test

## To access the application
* Method POST
  + url: http://localhost:8080/productapi/product
  + template: 
```
{
    "name": "Product name",
    "description": "Product description",
    "images": [
        {
            "type": "Type of the image 1"
        },
        {
            "type": "Type of the image 2"
        }
    ],
   "products": [
        {
            "name": "Product child name 1",
            "description": "Product child description 1"
        },
        {
            "name": "Product child name 2",
            "description": "Product child description 2"
        }
    ]
}
```

* Method PUT
  + url: http://localhost:8080/productapi/product
  + template: 
```
{
    "id": 1,
    "name": "Product name",
    "description": "Product description",
    "images": [
        {
            "type": "Type of the image 1"
        },
        {
            "type": "Type of the image 2"
        }
    ],
   "products": [
        {
            "name": "Product child name 1",
            "description": "Product child description 1"
        },
        {
            "name": "Product child name 2",
            "description": "Product child description 2"
        }
    ]
}
```

* Method DELETE
  + url: http://localhost:8080/productapi/product/1

* Method GET
  + urls: 

 1. http://localhost:8080/productapi/product
 2. http://localhost:8080/productapi/product?showImages=true
 3. http://localhost:8080/productapi/product?showProducts=true
 4. http://localhost:8080/productapi/product?showImages=true&showProducts=true

 5. http://localhost:8080/productapi/product/1
 6. http://localhost:8080/productapi/product/1?showImages=true
 7. http://localhost:8080/productapi/product/1?showProducts=true
 8. http://localhost:8080/productapi/product/1?showImages=true&showProducts=true

 9. http://localhost:8080/productapi/productchild/1
 10. http://localhost:8080/productapi/image/1

