package com.product.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.product.api.entity.Image;

@Repository
public interface ImageJpaRepository extends JpaRepository<Image, Long> {

	 List<Image> findByProduct_id(Long productParentId);
	
}
