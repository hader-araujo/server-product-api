package com.product.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.product.api.entity.Product;

@Repository
public interface ProductJpaRepository extends JpaRepository<Product, Long> {

	 List<Product> findByParentProductIsNull();
	 List<Product> findByParentProduct_id(Long productParentId);
}
