package com.product.api.rest.controller.test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.product.api.rest.controller.ProductRestController;
import com.product.api.rest.controller.ProductRestControllerImpl;
import com.product.api.service.ProductService;
import com.product.api.service.dto.product.ProductDTOGet;
import com.product.api.service.exception.ReadException;
import com.product.api.service.exception.enums.ReadExceptionMessageEnum;

public class ProductRestControllerTest_FindBy {

	private ProductService service;
	private ProductRestController controller;
	private Long id = 1L;
	private String name = "My Name";
	
	@Before
	public void setup() {
		service = mock(ProductService.class);
		controller = new ProductRestControllerImpl(service);
	}

	@Test
	public void get_FindAllShouldReturnOKStatusWithData() throws ReadException {
		ProductDTOGet dtoToReturn = new ProductDTOGet();
		dtoToReturn.setId(id);
		dtoToReturn.setName(name);
		List<ProductDTOGet> productList = new ArrayList<>();
		productList.add(dtoToReturn);

		when(service.findAll(false, false)).thenReturn(productList);

		@SuppressWarnings("rawtypes")
		ResponseEntity responseEntity = controller.getAll(false, false);

		HttpStatus httpStatus = responseEntity.getStatusCode();
		@SuppressWarnings("unchecked")
		List<ProductDTOGet> body = (List<ProductDTOGet>) responseEntity.getBody();

		assertThat("Wrong HTTP status for correct ID", httpStatus, equalTo(HttpStatus.OK));
		assertThat("Missing error code", body.get(0), hasProperty("id", (equalTo(id))));
		assertThat("Missing error code", body.get(0), hasProperty("name", (equalTo(name))));

		verify(service, times(1)).findAll(any(Boolean.class), any(Boolean.class));
	}
	
	@Test
	public void get_GivenExceptionOnServiceShouldReturnInternalServerErrorStatus() throws ReadException {
		when(service.findAll(any(Boolean.class), any(Boolean.class)))
				.thenThrow(new ReadException(ReadExceptionMessageEnum.UNEXPECTED_EXCEPTION));

		@SuppressWarnings("rawtypes")
		ResponseEntity responseEntity = controller.getAll(false, false);

		HttpStatus httpStatus = responseEntity.getStatusCode();
		assertThat("Wrong HTTP status", httpStatus, equalTo(HttpStatus.INTERNAL_SERVER_ERROR));
		verify(service, times(1)).findAll(any(Boolean.class), any(Boolean.class));
	}

	@Test
	public void findBy_GivenAnyExceptionShoudReturnInternalServerErrorStatus() throws ReadException {
		doThrow(new RuntimeException()).when(service).findAll(any(Boolean.class), any(Boolean.class));

		@SuppressWarnings("rawtypes")
		ResponseEntity responseEntity = controller.getAll(false, false);

		HttpStatus httpStatus = responseEntity.getStatusCode();
		assertThat("Wrong HTTP status", httpStatus, equalTo(HttpStatus.INTERNAL_SERVER_ERROR));
		verify(service, times(1)).findAll(any(Boolean.class), any(Boolean.class));
	}
}
