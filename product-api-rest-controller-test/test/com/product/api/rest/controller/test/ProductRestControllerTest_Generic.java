package com.product.api.rest.controller.test;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.product.api.rest.controller.ProductRestControllerImpl;

public class ProductRestControllerTest_Generic {

	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void generic_NullServiceShouldThrowException() {
		thrown.expect(RuntimeException.class);
		thrown.expectMessage("Service unavailable");
		
		new ProductRestControllerImpl(null);
	}
	
}
