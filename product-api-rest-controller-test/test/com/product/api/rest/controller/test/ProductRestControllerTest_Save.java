package com.product.api.rest.controller.test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import com.product.api.rest.controller.ProductRestController;
import com.product.api.rest.controller.ProductRestControllerImpl;
import com.product.api.service.ProductService;
import com.product.api.service.dto.product.ProductDTOPost;
import com.product.api.service.exception.CreateUpdateException;
import com.product.api.service.exception.enums.CreateUpdateExceptionMessageEnum;

public class ProductRestControllerTest_Save {

	private ProductService service;
	private ProductRestController controller;
	private BindingResult result;

	private String fieldName_Description = "description";
	private String fieldName_Name = "name";

	private String description = "myDescription";
	private String name = "My Name";

	@Before
	public void setup() {
		result = mock(BindingResult.class);
		service = mock(ProductService.class);
		controller = new ProductRestControllerImpl(service);
	}

	@Test
	public void save_GivenDTOShoudReturnOKStatus() throws CreateUpdateException {

		ProductDTOPost dto = new ProductDTOPost();
		dto.setDescription(description);
		dto.setName(name);

		when(result.hasErrors()).thenReturn(false);

		@SuppressWarnings("rawtypes")
		ResponseEntity responseEntity = controller.save(dto, result);

		HttpStatus httpStatus = responseEntity.getStatusCode();
		assertThat("Wrong HTTP status", httpStatus, equalTo(HttpStatus.CREATED));

		verify(service, times(1)).saveAndFlush(dto);
	}

	@Test
	public void save_GivenSomeErrorsShouldReturnTheCodeForEachField() throws CreateUpdateException {
		String errorCodeDescriptionNotNull = "NotNull.productDTO.description";
		String errorCodeNameSize = "Size.productDTO.name";

		List<ObjectError> listObjectError = new ArrayList<>();
		listObjectError.add(new ObjectError(fieldName_Description, new String[] { errorCodeDescriptionNotNull }, null, null));
		listObjectError.add(new ObjectError(fieldName_Name, new String[] { errorCodeNameSize }, null, null));

		when(result.hasErrors()).thenReturn(true);
		when(result.getAllErrors()).thenReturn(listObjectError);

		@SuppressWarnings({ "rawtypes" })
		ResponseEntity responseEntity = controller.save(null, result);

		HttpStatus httpStatus = responseEntity.getStatusCode();
		Object[] body = (Object[]) responseEntity.getBody();

		assertThat("Wrong HTTP status", httpStatus, equalTo(HttpStatus.BAD_REQUEST));
		assertThat("Missing error code", Arrays.asList(body), hasItem(equalTo(errorCodeDescriptionNotNull)));
		assertThat("Missing error code", Arrays.asList(body), hasItem(equalTo(errorCodeNameSize)));

		verify(service, times(0)).saveAndFlush(any(ProductDTOPost.class));
	}

	@Test
	public void save_GivenExceptionOnServiceShoudReturnInternalServerErrorStatus() throws CreateUpdateException {
		doThrow(new CreateUpdateException(CreateUpdateExceptionMessageEnum.UNEXPECTED_EXCEPTION)).when(service)
				.saveAndFlush(any(ProductDTOPost.class));
		when(result.hasErrors()).thenReturn(false);

		@SuppressWarnings("rawtypes")
		ResponseEntity responseEntity = controller.save(null, result);

		HttpStatus httpStatus = responseEntity.getStatusCode();
		assertThat("Wrong HTTP status", httpStatus, equalTo(HttpStatus.INTERNAL_SERVER_ERROR));
		verify(service, times(1)).saveAndFlush(any(ProductDTOPost.class));
	}

	@Test
	public void save_GivenAnyExceptionShoudReturnInternalServerErrorStatus() throws CreateUpdateException {
		doThrow(new RuntimeException()).when(result).hasErrors();

		@SuppressWarnings("rawtypes")
		ResponseEntity responseEntity = controller.save(null, result);

		HttpStatus httpStatus = responseEntity.getStatusCode();
		assertThat("Wrong HTTP status", httpStatus, equalTo(HttpStatus.INTERNAL_SERVER_ERROR));
		verify(service, times(0)).saveAndFlush(any(ProductDTOPost.class));
	}

}
