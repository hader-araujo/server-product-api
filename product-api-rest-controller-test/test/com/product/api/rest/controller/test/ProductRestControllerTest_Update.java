package com.product.api.rest.controller.test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import com.product.api.rest.controller.ProductRestController;
import com.product.api.rest.controller.ProductRestControllerImpl;
import com.product.api.service.ProductService;
import com.product.api.service.dto.product.ProductDTOPost;
import com.product.api.service.dto.product.ProductDTOPut;
import com.product.api.service.exception.CreateUpdateException;
import com.product.api.service.exception.enums.CreateUpdateExceptionMessageEnum;

public class ProductRestControllerTest_Update {

	private ProductService service;
	private ProductRestController controller;
	private BindingResult result;

	private String fieldName_Description = "description";
	private String fieldName_Name = "name";

	private Long id = 1L;
	private String description = "myDescription";
	private String name = "My Name";

	private String newDescription = "myOtherDescription";
	private String newName = "My other Name";

	@Before
	public void setup() {
		result = mock(BindingResult.class);
		service = mock(ProductService.class);
		controller = new ProductRestControllerImpl(service);
	}

	@Test
	public void update_GivenDTOShoudReturnOKStatus() throws CreateUpdateException {

		ProductDTOPut dto = new ProductDTOPut();
		dto.setId(id);
		dto.setDescription(newDescription);
		dto.setName(newName);

		when(result.hasErrors()).thenReturn(false);

		ProductDTOPost dtoToCheck = new ProductDTOPost();
		dtoToCheck.setId(id);
		dtoToCheck.setDescription(newDescription);
		dtoToCheck.setName(newName);

		ProductDTOPost dtoToBeChanged = new ProductDTOPost();
		dtoToBeChanged.setId(id);
		dtoToBeChanged.setDescription(description);
		dtoToBeChanged.setName(name);
		doAnswer(new Answer<ProductDTOPut>() {

			public ProductDTOPut answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				if (args[0] instanceof ProductDTOPut) {
					ProductDTOPut productDtoParam = (ProductDTOPut) args[0];
					productDtoParam.setDescription(newDescription);
					productDtoParam.setName(newName);
				}
				return null;
			}
		}).when(service).saveAndFlush(dtoToBeChanged);

		@SuppressWarnings("rawtypes")
		ResponseEntity responseEntity = controller.update(dto, result);

		HttpStatus httpStatus = responseEntity.getStatusCode();

		assertThat("Wrong HTTP status", httpStatus, equalTo(HttpStatus.NO_CONTENT));

		verify(service, times(1)).saveAndFlush(dtoToCheck);
	}

	@Test
	public void update_GivenDTOWithoutIdShoudReturnBadRequest() throws CreateUpdateException {
		String errorCodeIdNotNull = "NotNull.productDTO.id";
		when(result.hasErrors()).thenReturn(false);
		ProductDTOPut dto = new ProductDTOPut();
		dto.setDescription(description);
		dto.setName(name);

		@SuppressWarnings("rawtypes")
		ResponseEntity responseEntity = controller.update(dto, result);

		HttpStatus httpStatus = responseEntity.getStatusCode();
		Object[] body = (Object[]) responseEntity.getBody();

		assertThat("Wrong HTTP status", httpStatus, equalTo(HttpStatus.BAD_REQUEST));
		assertThat("Missing error code", Arrays.asList(body), hasItem(equalTo(errorCodeIdNotNull)));

		verify(service, times(0)).saveAndFlush(any(ProductDTOPost.class));
	}

	@Test
	public void update_GivenSomeErrorsShouldReturnTheCodeForEachField() throws CreateUpdateException {

		String errorCodeDescriptionNotNull = "NotNull.productDTO.description";
		String errorCodeNameSize = "Size.productDTO.name";

		List<ObjectError> listObjectError = new ArrayList<>();
		listObjectError.add(new ObjectError(fieldName_Description, new String[] { errorCodeDescriptionNotNull }, null, null));
		listObjectError.add(new ObjectError(fieldName_Name, new String[] { errorCodeNameSize }, null, null));

		when(result.hasErrors()).thenReturn(true);
		when(result.getAllErrors()).thenReturn(listObjectError);

		@SuppressWarnings({ "rawtypes" })
		ResponseEntity responseEntity = controller.update(new ProductDTOPut(), result);

		HttpStatus httpStatus = responseEntity.getStatusCode();
		Object[] body = (Object[]) responseEntity.getBody();

		assertThat("Wrong HTTP status", httpStatus, equalTo(HttpStatus.BAD_REQUEST));
		assertThat("Missing error code", Arrays.asList(body), hasItem(equalTo(errorCodeDescriptionNotNull)));
		assertThat("Missing error code", Arrays.asList(body), hasItem(equalTo(errorCodeNameSize)));

		verify(service, times(0)).saveAndFlush(any(ProductDTOPost.class));
	}

	@Test
	public void update_GivenExceptionOnServiceShoudReturnInternalServerErrorStatus() throws CreateUpdateException {
		doThrow(new CreateUpdateException(CreateUpdateExceptionMessageEnum.UNEXPECTED_EXCEPTION)).when(service)
				.saveAndFlush(any(ProductDTOPost.class));
		when(result.hasErrors()).thenReturn(false);

		ProductDTOPut productPut = new ProductDTOPut();
		productPut.setId(id);
		@SuppressWarnings("rawtypes")
		ResponseEntity responseEntity = controller.update(productPut, result);

		HttpStatus httpStatus = responseEntity.getStatusCode();
		assertThat("Wrong HTTP status", httpStatus, equalTo(HttpStatus.INTERNAL_SERVER_ERROR));
		verify(service, times(1)).saveAndFlush(any(ProductDTOPost.class));
	}

	@Test
	public void update_GivenAnyExceptionShoudReturnInternalServerErrorStatus() throws CreateUpdateException {
		doThrow(new RuntimeException()).when(result).hasErrors();

		@SuppressWarnings("rawtypes")
		ResponseEntity responseEntity = controller.update(new ProductDTOPut(), result);

		HttpStatus httpStatus = responseEntity.getStatusCode();
		assertThat("Wrong HTTP status", httpStatus, equalTo(HttpStatus.INTERNAL_SERVER_ERROR));
		verify(service, times(0)).saveAndFlush(any(ProductDTOPost.class));
	}

}
