package com.product.api.rest.controller;

import org.springframework.http.ResponseEntity;

public interface ImageRestController {

	@SuppressWarnings("rawtypes")
	ResponseEntity get(Long productParentId);
}