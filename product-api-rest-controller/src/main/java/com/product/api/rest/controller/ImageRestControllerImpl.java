package com.product.api.rest.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.product.api.service.ImageService;
import com.product.api.service.dto.image.ImageDTOGet;
import com.product.api.service.exception.ReadException;
import com.product.api.service.exception.enums.ReadExceptionMessageEnum;

@RestController
@RequestMapping(value = "/image")
public class ImageRestControllerImpl implements ImageRestController {

	private static final Logger log = LogManager.getLogger(ImageRestControllerImpl.class.getName());

	private ImageService service;

	@Autowired
	public ImageRestControllerImpl(ImageService service) {
		if (service == null) {
			throw new RuntimeException("Service unavailable");
		}
		this.service = service;
	}


	@Override
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{productParentId}", method = RequestMethod.GET)
	public ResponseEntity get(@PathVariable("productParentId") Long productParentId) {
		try {
			List<ImageDTOGet> dto = service.get(productParentId);

			return new ResponseEntity<List<ImageDTOGet>>(dto, HttpStatus.OK);

		} catch (ReadException e) {
			switch (ReadExceptionMessageEnum.valueOf(e.getMessage())) {
			case NOT_FOUND:
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			case ID_NULL_EXCEPTION:
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			case UNEXPECTED_EXCEPTION:
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			default:
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}

		} catch (Exception e) {
			log.error("get::Unexpected error on rest controller", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
