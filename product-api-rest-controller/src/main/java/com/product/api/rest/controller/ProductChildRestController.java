package com.product.api.rest.controller;

import org.springframework.http.ResponseEntity;

public interface ProductChildRestController {

	@SuppressWarnings("rawtypes")
	ResponseEntity get(Long productParentId);
}