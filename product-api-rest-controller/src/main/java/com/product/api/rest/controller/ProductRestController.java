package com.product.api.rest.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import com.product.api.service.dto.product.ProductDTOPost;
import com.product.api.service.dto.product.ProductDTOPut;

public interface ProductRestController {

	@SuppressWarnings("rawtypes")
	ResponseEntity get(Long id, boolean showProducts, boolean showImages);

	@SuppressWarnings("rawtypes")
	ResponseEntity getAll(boolean showProducts, boolean showImages);

	@SuppressWarnings("rawtypes")
	ResponseEntity save(ProductDTOPost product, BindingResult result);

	@SuppressWarnings("rawtypes")
	ResponseEntity update(ProductDTOPut product, BindingResult result);

	@SuppressWarnings("rawtypes")
	ResponseEntity delete(Long id);
}