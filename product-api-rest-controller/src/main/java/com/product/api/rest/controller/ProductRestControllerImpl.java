package com.product.api.rest.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.product.api.service.ProductService;
import com.product.api.service.dto.product.ProductDTOGet;
import com.product.api.service.dto.product.ProductDTOPost;
import com.product.api.service.dto.product.ProductDTOPut;
import com.product.api.service.exception.CreateUpdateException;
import com.product.api.service.exception.DeleteException;
import com.product.api.service.exception.ReadException;
import com.product.api.service.exception.enums.DeleteExceptionMessageEnum;
import com.product.api.service.exception.enums.ReadExceptionMessageEnum;

@RestController
@RequestMapping(value = "/product")
public class ProductRestControllerImpl implements ProductRestController {

	private static final Logger log = LogManager.getLogger(ProductRestControllerImpl.class.getName());

	private ProductService service;

	@Autowired
	public ProductRestControllerImpl(ProductService service) {
		if (service == null) {
			throw new RuntimeException("Service unavailable");
		}
		this.service = service;
	}

	@Override
	@SuppressWarnings("rawtypes")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity save(@RequestBody @Validated ProductDTOPost product, BindingResult result) {
		try {
			if (result.hasErrors()) {
				return ResponseEntity.badRequest()
						.body(result.getAllErrors().stream().map(error -> error.getCodes()[0]).toArray());
			}

			ProductDTOGet productDTOGet = service.saveAndFlush(product);

			return new ResponseEntity<>(productDTOGet, HttpStatus.CREATED);
		} catch (CreateUpdateException e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			log.error("save::Unexpected error on rest controller", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity get(@PathVariable("id") Long id,
			@RequestParam(value = "showProducts", required = false, defaultValue = "false") boolean showProducts,
			@RequestParam(value = "showImages", required = false, defaultValue = "false") boolean showImages) {
		try {
			ProductDTOGet dto = service.findOne(id, showProducts, showImages);

			return new ResponseEntity<ProductDTOGet>(dto, HttpStatus.OK);

		} catch (ReadException e) {
			switch (ReadExceptionMessageEnum.valueOf(e.getMessage())) {
			case NOT_FOUND:
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			case ID_NULL_EXCEPTION:
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			case UNEXPECTED_EXCEPTION:
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			default:
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}

		} catch (Exception e) {
			log.error("get::Unexpected error on rest controller", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@SuppressWarnings({"rawtypes" })
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity getAll(
			@RequestParam(value = "showProducts", required = false, defaultValue = "false") boolean showProducts,
			@RequestParam(value = "showImages", required = false, defaultValue = "false") boolean showImages) {
		try {
			List<ProductDTOGet> dtoList = service.findAll(showProducts, showImages);

			return new ResponseEntity<List<ProductDTOGet>>(dtoList, HttpStatus.OK);

		} catch (ReadException e) {
			log.error("findBy:: Error getting the user page", e);
			return new ResponseEntity<List<ProductDTOGet>>(HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			log.error("findBy::Unexpected error on rest controller", e);
			return new ResponseEntity<List<ProductDTOGet>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@SuppressWarnings("rawtypes")
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity update(@RequestBody @Validated ProductDTOPut product, BindingResult result) {

		try {
			if (result.hasErrors()) {
				return ResponseEntity.badRequest()
						.body(result.getAllErrors().stream().map(error -> error.getCodes()[0]).toArray());
			}

			Assert.notNull(product.getId(), "ID should not be null");

			service.saveAndFlush(new ProductDTOPost(product));

			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (CreateUpdateException e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (IllegalArgumentException e) {
			if ("ID should not be null".equals(e.getMessage())) {
				return new ResponseEntity<>(new Object[] { "NotNull.productDTO.id" }, HttpStatus.BAD_REQUEST);
			} else {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			log.error("update::Unexpected error on rest controller", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity delete(@PathVariable("id") Long id) {

		// TODO tests cases need to be written

		try {
			service.delete(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);

		} catch (DeleteException e) {
			if (DeleteExceptionMessageEnum.valueOf(e.getMessage())== DeleteExceptionMessageEnum.NOT_FOUND)
			{
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);				
			}else{
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}

		} catch (Exception e) {
			log.error("get::Unexpected error on rest controller", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
