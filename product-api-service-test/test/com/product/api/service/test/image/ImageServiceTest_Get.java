package com.product.api.service.test.image;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.product.api.entity.Image;
import com.product.api.repository.ImageJpaRepository;
import com.product.api.service.ImageService;
import com.product.api.service.ImageServiceImpl;
import com.product.api.service.dto.image.ImageDTOGet;
import com.product.api.service.exception.ReadException;
import com.product.api.service.exception.enums.ReadExceptionMessageEnum;

public class ImageServiceTest_Get {

	private ImageJpaRepository repository;
	private ImageService service;

	private final long productid = 1;
	
	private String propertyId = "id";
	private String propertyType = "type";
	
	private final long id1 = 1;
	private final String type1 = "type 1";

	private final long id2 = 2;
	private final String type2 = "type 2";

	private final long id3 = 3;
	private final String type3 = "type 3";

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Before
	public void setup() {
		repository = mock(ImageJpaRepository.class);
		service = new ImageServiceImpl(repository);
	}

	@Test
	public void Get_GivenEntityListShouldReturnDtoList() throws ReadException {

		List<Image> imageList = getListOfImage();

		when(repository.findByProduct_id(productid)).thenReturn(imageList);

		List<ImageDTOGet> imageDtoListExpected = getListOfImageDTO();
		List<ImageDTOGet> imageDtoListOut = service.get(productid);

		assertThat("The list should not be empty", imageDtoListOut, not(empty()));
		assertThat("The list should has 3 elements", imageDtoListOut, hasSize(3));
		assertThat("The return is wrong", imageDtoListOut, is(imageDtoListExpected));
		for (ImageDTOGet dtoExpected : imageDtoListExpected){
			assertThat("The return is wrong", imageDtoListOut, hasItem( hasProperty(propertyId, equalTo(dtoExpected.getId()))));
			assertThat("The return is wrong", imageDtoListOut, hasItem(hasProperty(propertyType, equalTo(dtoExpected.getType()))));
			
		}
	}

	@Test
	public void Get_NoneDtoShouldReturnEmptyList() throws ReadException {
		when(repository.findByProduct_id(productid)).thenReturn(Collections.emptyList());

		List<ImageDTOGet> imageList = service.get(productid);

		assertThat("The list should be empty but not null", imageList, both(empty()).and(notNullValue()));
	}

	@Test
	public void Get_GivenExceptionOnRepositoryShouldThowException() throws ReadException {
		thrown.expect(ReadException.class);
		thrown.expectMessage(ReadExceptionMessageEnum.UNEXPECTED_EXCEPTION.name());

		when(repository.findByProduct_id(any(Long.class))).thenThrow(new RuntimeException());

		service.get(productid);
	}

	private List<Image> getListOfImage() {
		Image image1 = new Image();
		image1.setId(id1);
		image1.setType(type1);

		Image image2 = new Image();
		image2.setId(id2);
		image2.setType(type2);

		Image image3 = new Image();
		image3.setId(id3);
		image3.setType(type3);

		List<Image> imageList = new ArrayList<>();
		imageList.add(image1);
		imageList.add(image2);
		imageList.add(image3);

		return imageList;
	}

	private List<ImageDTOGet> getListOfImageDTO() {
		ImageDTOGet imageDto1 = new ImageDTOGet();
		imageDto1.setId(id1);
		imageDto1.setType(type1);

		ImageDTOGet imageDto2 = new ImageDTOGet();
		imageDto2.setId(id2);
		imageDto2.setType(type2);

		ImageDTOGet imageDto3 = new ImageDTOGet();
		imageDto3.setId(id3);
		imageDto3.setType(type3);

		List<ImageDTOGet> imageDtoList = new ArrayList<>();
		imageDtoList.add(imageDto1);
		imageDtoList.add(imageDto2);
		imageDtoList.add(imageDto3);

		return imageDtoList;
	}
}
