package com.product.api.service.test.product;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.product.api.entity.Product;
import com.product.api.repository.ProductJpaRepository;
import com.product.api.service.ProductService;
import com.product.api.service.ProductServiceImpl;
import com.product.api.service.exception.DeleteException;
import com.product.api.service.exception.ReadException;
import com.product.api.service.exception.enums.DeleteExceptionMessageEnum;
import com.product.api.service.exception.enums.ReadExceptionMessageEnum;

public class ProductServiceTest_Delete {

	private ProductJpaRepository repository;
	private ProductService service;

	private final Long id = 1L;

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Before
	public void setup() {
		repository = mock(ProductJpaRepository.class);
		service = new ProductServiceImpl(repository);
	}

	@Test
	public void delete_GivenOneEntityShouldDeleteIt() throws ReadException, DeleteException {
		thrown.expect(ReadException.class);
		thrown.expectMessage(ReadExceptionMessageEnum.NOT_FOUND.name());

		Product entity = new Product();
		entity.setId(id);
		when(repository.findOne(id)).thenReturn(entity).thenReturn(null);

		service.delete(id);
		service.findOne(id, false, false);

		verify(service, times(1)).delete(id);
		verify(service, times(2)).findOne(id, any(Boolean.class), any(Boolean.class));

	}

	@Test
	public void delete_DontExistEntityShouldThrowException() throws DeleteException {
		thrown.expect(DeleteException.class);
		thrown.expectMessage(DeleteExceptionMessageEnum.NOT_FOUND.name());

		when(repository.findOne(id)).thenReturn(null);

		service.delete(id);
	}

	@Test
	public void delete_NullIdShouldThrowException() throws DeleteException {
		thrown.expect(DeleteException.class);
		thrown.expectMessage(DeleteExceptionMessageEnum.ID_NULL_EXCEPTION.name());

		when(repository.findOne(any(Long.class))).thenThrow(new IllegalArgumentException());

		service.delete(null);
	}

	@Test
	public void delete_GivenExceptionOnRepositoryShouldThowException() throws DeleteException {
		thrown.expect(DeleteException.class);
		thrown.expectMessage(ReadExceptionMessageEnum.UNEXPECTED_EXCEPTION.name());

		Product entity = new Product();
		entity.setId(id);
		when(repository.findOne(id)).thenReturn(entity);

		doThrow(new RuntimeException()).when(repository).delete(any(Long.class));

		service.delete(id);
	}

}
