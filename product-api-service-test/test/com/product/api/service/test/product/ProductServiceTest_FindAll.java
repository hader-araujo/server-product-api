package com.product.api.service.test.product;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.product.api.entity.Product;
import com.product.api.repository.ProductJpaRepository;
import com.product.api.service.ProductService;
import com.product.api.service.ProductServiceImpl;
import com.product.api.service.dto.product.ProductDTOGet;
import com.product.api.service.exception.ReadException;
import com.product.api.service.exception.enums.ReadExceptionMessageEnum;

public class ProductServiceTest_FindAll {

	private ProductJpaRepository repository;
	private ProductService service;

	private String propertyId = "id";
	private String propertyDescription = "description";
	private String propertyName = "name";
	
	private final long id1 = 1;
	private final String description1 = "description_1";
	private final String name1 = "The name of the first person";

	private final long id2 = 2;
	private final String description2 = "description_2";
	private final String name2 = "The name of the second person";

	private final long id3 = 3;
	private final String description3 = "description_3";
	private final String name3 = "The name of the third person";

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Before
	public void setup() {
		repository = mock(ProductJpaRepository.class);
		service = new ProductServiceImpl(repository);
	}

	@Test
	public void findAll_GivenEntityListShouldReturnDtoList() throws ReadException {

		List<Product> productList = getListOfProduct();

		when(repository.findByParentProductIsNull()).thenReturn(productList);

		List<ProductDTOGet> productDtoListExpected = getListOfProductDTO();
		List<ProductDTOGet> productDtoListOut = service.findAll(false, false);

		assertThat("The list should not be empty", productDtoListOut, not(empty()));
		assertThat("The list should has 3 elements", productDtoListOut, hasSize(3));
		assertThat("The return is wrong", productDtoListOut, is(productDtoListExpected));
		for (ProductDTOGet dtoExpected : productDtoListExpected){
			assertThat("The return is wrong", productDtoListOut, hasItem( hasProperty(propertyId, equalTo(dtoExpected.getId()))));
			assertThat("The return is wrong", productDtoListOut, hasItem(hasProperty(propertyDescription, equalTo(dtoExpected.getDescription()))));
			assertThat("The return is wrong", productDtoListOut, hasItem(hasProperty(propertyName, equalTo(dtoExpected.getName()))));
			
		}
	}

	@Test
	public void findAll_NoneDtoShouldReturnEmptyList() throws ReadException {
		when(repository.findAll()).thenReturn(Collections.emptyList());

		List<ProductDTOGet> productList = service.findAll(false, false);

		assertThat("The list should be empty but not null", productList, both(empty()).and(notNullValue()));
	}

	@Test
	public void findOne_GivenExceptionOnRepositoryShouldThowException() throws ReadException {
		thrown.expect(ReadException.class);
		thrown.expectMessage(ReadExceptionMessageEnum.UNEXPECTED_EXCEPTION.name());

		when(repository.findByParentProductIsNull()).thenThrow(new RuntimeException());

		service.findAll(false, false);
	}

	private List<Product> getListOfProduct() {
		Product product1 = new Product();
		product1.setId(id1);
		product1.setDescription(description1);
		product1.setName(name1);

		Product product2 = new Product();
		product2.setId(id2);
		product2.setDescription(description2);
		product2.setName(name2);

		Product product3 = new Product();
		product3.setId(id3);
		product3.setDescription(description3);
		product3.setName(name3);

		List<Product> productList = new ArrayList<>();
		productList.add(product1);
		productList.add(product2);
		productList.add(product3);

		return productList;
	}

	private List<ProductDTOGet> getListOfProductDTO() {
		ProductDTOGet productDto1 = new ProductDTOGet();
		productDto1.setId(id1);
		productDto1.setDescription(description1);
		productDto1.setName(name1);

		ProductDTOGet productDto2 = new ProductDTOGet();
		productDto2.setId(id2);
		productDto2.setDescription(description2);
		productDto2.setName(name2);

		ProductDTOGet productDto3 = new ProductDTOGet();
		productDto3.setId(id3);
		productDto3.setDescription(description3);
		productDto3.setName(name3);

		List<ProductDTOGet> productDtoList = new ArrayList<>();
		productDtoList.add(productDto1);
		productDtoList.add(productDto2);
		productDtoList.add(productDto3);

		return productDtoList;
	}
}
