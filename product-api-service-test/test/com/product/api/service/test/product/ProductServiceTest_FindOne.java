package com.product.api.service.test.product;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.product.api.entity.Image;
import com.product.api.entity.Product;
import com.product.api.repository.ProductJpaRepository;
import com.product.api.service.ProductService;
import com.product.api.service.ProductServiceImpl;
import com.product.api.service.dto.product.ProductDTOGet;
import com.product.api.service.exception.ReadException;
import com.product.api.service.exception.enums.ReadExceptionMessageEnum;

public class ProductServiceTest_FindOne {

	private ProductJpaRepository repository;
	private ProductService service;

	final Long id = 1L;
	final String name = "myName";
	final String nameProductChild = "Other product name";
	final String imageType = "The image type";

	private String propertyId = "id";
	private String propertyName = "name";
	private String propertyType = "type";

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Before
	public void setup() {
		repository = mock(ProductJpaRepository.class);
		service = new ProductServiceImpl(repository);
	}

	@Test
	public void findOne_GivenOneEntityShouldReturnOneDtoWithImagesAndProducts() throws ReadException {

		List<Image> imageList = new ArrayList<>();
		Image image = new Image();
		image.setId(id);
		image.setType(imageType);
		imageList.add(image);

		List<Product> productList = new ArrayList<>();
		Product productChild = new Product();
		productChild.setId(id);
		productChild.setName(nameProductChild);
		productList.add(productChild);

		Product entity = new Product();
		entity.setId(id);
		entity.setName(name);
		entity.setImages(imageList);
		entity.setProducts(productList);
		when(repository.findOne(id)).thenReturn(entity);

		ProductDTOGet dto = service.findOne(id, true, true);

		assertThat("DTO should not be null", dto, notNullValue());
		assertThat("Service should return the id", dto.getId(), equalTo(id));
		assertThat("Service should return the name", dto.getName(), equalTo(name));

		assertThat("Service should return the child images", dto.getImages(),
				hasItem(hasProperty(propertyId, equalTo(id))));
		assertThat("Service should return the child images", dto.getImages(),
				hasItem(hasProperty(propertyType, equalTo(imageType))));

		assertThat("Service should return the child products", dto.getProducts(),
				hasItem(hasProperty(propertyId, equalTo(id))));
		assertThat("Service should return the child products", dto.getProducts(),
				hasItem(hasProperty(propertyName, equalTo(nameProductChild))));

	}

	@Test
	public void findOne_GivenOneEntityShouldReturnOneDtoWithoutImagesAndProducts() throws ReadException {

		Product entity = new Product();
		entity.setId(id);
		entity.setName(name);
		when(repository.findOne(id)).thenReturn(entity);

		ProductDTOGet dto = service.findOne(id, false, false);

		assertThat("DTO should not be null", dto, notNullValue());
		assertThat("Service should return the id", dto.getId(), equalTo(id));
		assertThat("Service should return the name", dto.getName(), equalTo(name));
		assertThat("Service should not return the child images", dto.getImages(), is(nullValue()));
		assertThat("Service should not return the child products", dto.getProducts(), is(nullValue()));
	}

	@Test
	public void findOne_DontExistEntityShouldThrowException() throws ReadException {
		thrown.expect(ReadException.class);
		thrown.expectMessage(ReadExceptionMessageEnum.NOT_FOUND.name());

		when(repository.findOne(id)).thenReturn(null);

		service.findOne(id, false, false);
	}

	@Test
	public void findOne_NullIdShouldThrowException() throws ReadException {
		thrown.expect(ReadException.class);
		thrown.expectMessage(ReadExceptionMessageEnum.ID_NULL_EXCEPTION.name());

		when(repository.findOne(any(Long.class))).thenThrow(new IllegalArgumentException());

		service.findOne(null, false, false);
	}

	@Test
	public void findOne_GivenExceptionOnRepositoryShouldThowException() throws ReadException {
		thrown.expect(ReadException.class);
		thrown.expectMessage(ReadExceptionMessageEnum.UNEXPECTED_EXCEPTION.name());

		when(repository.findOne(any(Long.class))).thenThrow(new RuntimeException());

		service.findOne(id, false, false);
	}

}
