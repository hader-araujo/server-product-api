package com.product.api.service.test.product;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.product.api.service.ProductServiceImpl;

public class ProductServiceTest_Generic {

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void generic_NullRepositotyShouldThrowException() {
		thrown.expect(RuntimeException.class);
		thrown.expectMessage("Repository unavailable");

		new ProductServiceImpl(null);
	}
}
