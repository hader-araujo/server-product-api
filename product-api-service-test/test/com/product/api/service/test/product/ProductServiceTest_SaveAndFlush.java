package com.product.api.service.test.product;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.product.api.entity.Product;
import com.product.api.repository.ProductJpaRepository;
import com.product.api.service.ProductService;
import com.product.api.service.ProductServiceImpl;
import com.product.api.service.dto.product.ProductDTOPost;
import com.product.api.service.exception.CreateUpdateException;
import com.product.api.service.exception.ReadException;
import com.product.api.service.exception.enums.CreateUpdateExceptionMessageEnum;

public class ProductServiceTest_SaveAndFlush {

	private ProductJpaRepository repository;
	private ProductService service;

	private final Long id = 1L;
	private final String description = "myDescription";
	private final String name = "myName";
	private final String newDescription = "MY NEW DESCRIPTION";
	private final String newName = "MY NEW NAME";

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Before
	public void setup() {
		repository = mock(ProductJpaRepository.class);
		service = new ProductServiceImpl(repository);
	}

	@Test
	public void saveAndFlush_GivenEntityWithIdShouldUpdate() throws CreateUpdateException, ReadException {
		Product productSearch = new Product();
		productSearch.setId(id);
		productSearch.setDescription(description);
		productSearch.setName(name);

		Product productUpdated = new Product();
		productUpdated.setId(id);
		productUpdated.setDescription(newDescription);
		productUpdated.setName(newName);

		when(repository.findOne(id)).thenReturn(productSearch);
		when(repository.saveAndFlush(productUpdated)).thenReturn(productUpdated);

		ProductDTOPost productDTO = new ProductDTOPost();
		productDTO.setId(id);
		productDTO.setDescription(newDescription);
		productDTO.setName(newName);
		service.saveAndFlush(productDTO);

		assertThat("DTO should has id", productDTO, hasProperty("id", equalTo(id)));
		assertThat("DTO should has description", productDTO, hasProperty("description", equalTo(newDescription)));
		assertThat("DTO should has name", productDTO, hasProperty("name", equalTo(newName)));
		
		verify(repository, times(1)).saveAndFlush(productUpdated);
	}

	@Test
	public void saveAndFlush_GivenEntityWithoutIdShouldSaveDto() throws CreateUpdateException {
		Product product = new Product();
		product.setDescription(description.toUpperCase());
		product.setName(name.toUpperCase());

		when(repository.saveAndFlush(product)).thenReturn(product);
		when(repository.findOne(product.getId())).thenReturn(product);

		ProductDTOPost dto = new ProductDTOPost();
		dto.setDescription(description);
		dto.setName(name);

		service.saveAndFlush(dto);

		verify(repository, times(1)).saveAndFlush(product);

		assertThat("DTO should has description", dto, hasProperty("description", equalTo(description)));
		assertThat("DTO should has name", dto, hasProperty("name", equalTo(name)));
	}

	@Test
	public void findOne_GivenExceptionOnRepositoryShouldThowException() throws CreateUpdateException {
		thrown.expect(CreateUpdateException.class);
		thrown.expectMessage(CreateUpdateExceptionMessageEnum.UNEXPECTED_EXCEPTION.name());

		when(repository.saveAndFlush(any(Product.class))).thenThrow(new RuntimeException());

		service.saveAndFlush(new ProductDTOPost());
	}

}
