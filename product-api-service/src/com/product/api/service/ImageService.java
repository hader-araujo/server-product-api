package com.product.api.service;

import java.util.List;

import com.product.api.service.dto.image.ImageDTOGet;
import com.product.api.service.exception.ReadException;

public interface ImageService {

	List<ImageDTOGet> get(Long productParentId) throws ReadException;

}