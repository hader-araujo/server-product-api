package com.product.api.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.product.api.entity.Image;
import com.product.api.repository.ImageJpaRepository;
import com.product.api.service.dto.image.ImageDTOGet;
import com.product.api.service.exception.ReadException;
import com.product.api.service.exception.enums.ReadExceptionMessageEnum;

@Service
@Transactional(readOnly = true)
public class ImageServiceImpl implements ImageService {

	private static final Logger log = LogManager.getLogger(ImageServiceImpl.class.getName());

	private ImageJpaRepository repository;

	@Autowired
	public ImageServiceImpl(ImageJpaRepository repository) {
		if (repository == null) {
			throw new RuntimeException("Repository unavailable");
		}
		this.repository = repository;
	}

	@Override
	public List<ImageDTOGet> get(Long productParentId) throws ReadException {
		try {
			List<Image> imageList = repository.findByProduct_id(productParentId);
			return imageList.stream().map(f -> new ImageDTOGet(f))
					.collect(Collectors.toList());
		} catch (Exception e) {
			log.error(ReadExceptionMessageEnum.UNEXPECTED_EXCEPTION, e);
			throw new ReadException(ReadExceptionMessageEnum.UNEXPECTED_EXCEPTION, e);
		}
	}

}
