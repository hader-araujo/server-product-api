package com.product.api.service;

import java.util.List;

import com.product.api.service.dto.product.ProductDTOGet;
import com.product.api.service.dto.product.ProductDTOPost;
import com.product.api.service.exception.CreateUpdateException;
import com.product.api.service.exception.DeleteException;
import com.product.api.service.exception.ReadException;

public interface ProductService {

	ProductDTOGet findOne(Long id, boolean showProducts, boolean showImages) throws ReadException;

	ProductDTOGet saveAndFlush(ProductDTOPost dto) throws CreateUpdateException;

	List<ProductDTOGet> findAll(boolean showProducts, boolean showImages) throws ReadException;

	void delete(Long id) throws DeleteException;
	
	List<ProductDTOGet> get(Long productParentId) throws ReadException;

}