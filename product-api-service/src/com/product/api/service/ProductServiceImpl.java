package com.product.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.product.api.entity.Image;
import com.product.api.entity.Product;
import com.product.api.repository.ProductJpaRepository;
import com.product.api.service.dto.product.ProductDTOGet;
import com.product.api.service.dto.product.ProductDTOPost;
import com.product.api.service.exception.CreateUpdateException;
import com.product.api.service.exception.DeleteException;
import com.product.api.service.exception.ReadException;
import com.product.api.service.exception.enums.CreateUpdateExceptionMessageEnum;
import com.product.api.service.exception.enums.DeleteExceptionMessageEnum;
import com.product.api.service.exception.enums.ReadExceptionMessageEnum;

@Service
@Transactional(readOnly = true)
public class ProductServiceImpl implements ProductService {

	private static final Logger log = LogManager.getLogger(ProductServiceImpl.class.getName());

	private ProductJpaRepository repository;

	@Autowired
	public ProductServiceImpl(ProductJpaRepository repository) {
		if (repository == null) {
			throw new RuntimeException("Repository unavailable");
		}
		this.repository = repository;
	}

	@Override
	@Transactional(readOnly = false)
	public ProductDTOGet saveAndFlush(ProductDTOPost dto) throws CreateUpdateException {
		try {
			Product product = new Product();
			product.setId(dto.getId());
			product.setDescription(dto.getDescription().toUpperCase());
			product.setName(dto.getName().toUpperCase());

			List<Image> images;
			if (dto.getImages() != null) {
				images = new ArrayList<>();
				dto.getImages().stream().forEach(c -> {
					Image image = new Image();
					image.setId(c.getId());
					image.setType(c.getType());
					image.setProduct(product);
					images.add(image);
				});
			} else {
				images = null;
			}

			List<Product> products;
			if (dto.getProducts() != null) {
				products = new ArrayList<>();
				dto.getProducts().stream().forEach(c -> {
					Product childProduct = new Product();
					childProduct.setId(c.getId());
					childProduct.setName(c.getName());
					childProduct.setDescription(c.getDescription());
					childProduct.setParentProduct(product);
					products.add(childProduct);
				});
			} else {
				products = null;
			}

			product.setImages(images);
			product.setProducts(products);

			repository.saveAndFlush(product);

			return findOne(product.getId(), true, true);
		} catch (Exception e) {
			log.error(CreateUpdateExceptionMessageEnum.UNEXPECTED_EXCEPTION, e);
			throw new CreateUpdateException(CreateUpdateExceptionMessageEnum.UNEXPECTED_EXCEPTION, e);
		}
	}

	@Override
	public ProductDTOGet findOne(Long id, boolean showProducts, boolean showImages) throws ReadException {
		try {
			Product entity = repository.findOne(id);

			if (entity == null) {
				throw new ReadException(ReadExceptionMessageEnum.NOT_FOUND);
			}

			log.debug("findOne::Entity finded: " + entity);
			ProductDTOGet dto = new ProductDTOGet(entity, showProducts, showImages);
			return dto;

		} catch (ReadException e) {
			throw e;
		} catch (IllegalArgumentException e) {
			throw new ReadException(ReadExceptionMessageEnum.ID_NULL_EXCEPTION);
		} catch (Exception e) {
			log.error(ReadExceptionMessageEnum.UNEXPECTED_EXCEPTION, e);
			throw new ReadException(ReadExceptionMessageEnum.UNEXPECTED_EXCEPTION, e);
		}
	}

	@Override
	public List<ProductDTOGet> findAll(boolean showProducts, boolean showImages) throws ReadException {
		try {
			List<Product> productList = repository.findByParentProductIsNull();
			return productList.stream().map(f -> new ProductDTOGet(f, showProducts, showImages)).collect(Collectors.toList());
		} catch (Exception e) {
			log.error(ReadExceptionMessageEnum.UNEXPECTED_EXCEPTION, e);
			throw new ReadException(ReadExceptionMessageEnum.UNEXPECTED_EXCEPTION, e);
		}
	}

	@Override
	@Transactional(readOnly = false)
	public void delete(Long id) throws DeleteException {
		try {
			findOne(id, false, false);
			repository.delete(id);
		} catch (ReadException e) {
			if (e.getMessage().equals(ReadExceptionMessageEnum.ID_NULL_EXCEPTION.name())) {
				throw new DeleteException(DeleteExceptionMessageEnum.ID_NULL_EXCEPTION);
			} else if (e.getMessage().equals(ReadExceptionMessageEnum.NOT_FOUND.name())) {
				throw new DeleteException(DeleteExceptionMessageEnum.NOT_FOUND);
			} else {
				throw new DeleteException(DeleteExceptionMessageEnum.UNEXPECTED_EXCEPTION);
			}

		} catch (IllegalArgumentException e) {
			throw new DeleteException(DeleteExceptionMessageEnum.ID_NULL_EXCEPTION);
		} catch (Exception e) {
			log.error(DeleteExceptionMessageEnum.UNEXPECTED_EXCEPTION, e);
			throw new DeleteException(DeleteExceptionMessageEnum.UNEXPECTED_EXCEPTION, e);
		}
	}
	
	@Override
	public List<ProductDTOGet> get(Long productParentId) throws ReadException {
		try {
			List<Product> productList = repository.findByParentProduct_id(productParentId);
			return productList.stream().map(f -> new ProductDTOGet(f, false, false))
					.collect(Collectors.toList());
		} catch (Exception e) {
			log.error(ReadExceptionMessageEnum.UNEXPECTED_EXCEPTION, e);
			throw new ReadException(ReadExceptionMessageEnum.UNEXPECTED_EXCEPTION, e);
		}
	}

}
