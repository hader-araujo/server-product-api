package com.product.api.service.dto.image;

import com.product.api.service.dto.GenenricDTO;

public interface ImageDTO extends GenenricDTO{

	Long getId();

	void setId(Long id);

	String getType();

	void setType(String type);
}
