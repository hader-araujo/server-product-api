package com.product.api.service.dto.image;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.product.api.entity.Image;

public class ImageDTOGet implements ImageDTO {

	@JsonIgnore
	private static final long serialVersionUID = 4824005606745947284L;

	private Long id;
	private String type;

	public ImageDTOGet() {
	}

	public ImageDTOGet(Image image) {
		this.setId(image.getId());
		this.setType(image.getType());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImageDTOGet other = (ImageDTOGet) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ImageDTOGet [id=" + id + ", type=" + type + "]";
	}
}
