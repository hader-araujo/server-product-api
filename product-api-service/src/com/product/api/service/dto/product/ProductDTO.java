package com.product.api.service.dto.product;

import com.product.api.entity.Product;
import com.product.api.service.dto.GenenricDTO;

public interface ProductDTO extends GenenricDTO {
	Long getId();

	void setId(Long id);

	String getName();

	void setName(String name);

	String getDescription();

	void setDescription(String description);

	void setChildImages(Product product);

	void setChildProducts(Product product);

	default void productToDTO(Product product) {
		productToDTO(product, true, true);
	}

	default void productToDTO(Product product, boolean showProducts, boolean showImages) {
		this.setId(product.getId());
		this.setName(product.getName());
		this.setDescription(product.getDescription());
		if (showImages) {
			setChildImages(product);
		}
		if (showProducts) {
			setChildProducts(product);
		}
	}
}
