package com.product.api.service.dto.product;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.product.api.entity.Product;
import com.product.api.service.dto.image.ImageDTOGet;

public class ProductDTOGet implements ProductDTO {

	@JsonIgnore
	private static final long serialVersionUID = 4824005606745947284L;

	private Long id;
	private String name;
	private String description;

	private List<ImageDTOGet> images;
	private List<ProductDTOGet> products;

	public ProductDTOGet() {
	}

	public ProductDTOGet(Product product) {
		this(product, false, false);
	}

	public ProductDTOGet(Product product, boolean showProducts, boolean showImages) {
		productToDTO(product, showProducts, showImages);
	}

	@Override
	public void setChildImages(Product product) {
		if (product.getImages() != null) {
			this.setImages(new ArrayList<>());
			product.getImages().stream().forEach(c -> this.getImages().add(new ImageDTOGet(c)));
		}
	}

	@Override
	public void setChildProducts(Product product) {
		if (product.getProducts() != null) {
			this.setProducts(new ArrayList<>());
			product.getProducts().stream().forEach(c -> this.getProducts().add(new ProductDTOGet(c)));
		}
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<ImageDTOGet> getImages() {
		return images;
	}

	public void setImages(List<ImageDTOGet> images) {
		this.images = images;
	}

	public List<ProductDTOGet> getProducts() {
		return products;
	}

	public void setProducts(List<ProductDTOGet> products) {
		this.products = products;
	}

	@Override
	public String toString() {
		return "ProductDTOGet [id=" + id + ", name=" + name + ", description=" + description + ", images=" + images
				+ ", products=" + products + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((images == null) ? 0 : images.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((products == null) ? 0 : products.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductDTOGet other = (ProductDTOGet) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (images == null) {
			if (other.images != null)
				return false;
		} else if (!images.equals(other.images))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (products == null) {
			if (other.products != null)
				return false;
		} else if (!products.equals(other.products))
			return false;
		return true;
	}

}