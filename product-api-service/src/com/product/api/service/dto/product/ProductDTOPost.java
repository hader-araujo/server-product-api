package com.product.api.service.dto.product;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.product.api.entity.Product;
import com.product.api.service.dto.image.ImageDTOPost;

public class ProductDTOPost implements ProductDTO {

	@JsonIgnore
	private static final long serialVersionUID = 4824005606745947284L;

	@JsonIgnore
	private Long id;

	@NotNull
	@Size(min = 5, max = 60)
	private String name;

	@NotNull
	@Size(min = 5, max = 120)
	private String description;

	private List<ImageDTOPost> images;

	private List<ProductDTOPost> products;

	public ProductDTOPost() {
	}

	public ProductDTOPost(Product product) {
		productToDTO(product);
	}

	public ProductDTOPost(ProductDTOPut productDTOPut) {
		this.setId(productDTOPut.getId());
		this.setName(productDTOPut.getName());
		this.setDescription(productDTOPut.getDescription());
		setChildImages(productDTOPut);
		setChildProducts(productDTOPut);

	}

	@JsonIgnore
	@Override
	public void setChildImages(Product product) {
		if (product.getImages() != null) {
			this.setImages(new ArrayList<>());
			product.getImages().stream().forEach(c -> this.getImages().add(new ImageDTOPost(c)));
		}
	}

	@JsonIgnore
	@Override
	public void setChildProducts(Product product) {
		if (product.getProducts() != null) {
			this.setProducts(new ArrayList<>());
			product.getProducts().stream().forEach(c -> this.getProducts().add(new ProductDTOPost(c)));
		}
	}
	
	private void setChildImages(ProductDTOPut productDTOPut) {
		if (productDTOPut.getImages() != null) {
			this.setImages(new ArrayList<>());
			productDTOPut.getImages().stream().forEach(c -> this.getImages().add(new ImageDTOPost(c)));
		}
	}
	
	private void setChildProducts(ProductDTOPut productDTOPut) {
		if (productDTOPut.getProducts() != null) {
			this.setProducts(new ArrayList<>());
			productDTOPut.getProducts().stream().forEach(c -> this.getProducts().add(new ProductDTOPost(c)));
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<ImageDTOPost> getImages() {
		return images;
	}

	public void setImages(List<ImageDTOPost> images) {
		this.images = images;
	}

	public List<ProductDTOPost> getProducts() {
		return products;
	}

	public void setProducts(List<ProductDTOPost> products) {
		this.products = products;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((images == null) ? 0 : images.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((products == null) ? 0 : products.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductDTOPost other = (ProductDTOPost) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (images == null) {
			if (other.images != null)
				return false;
		} else if (!images.equals(other.images))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (products == null) {
			if (other.products != null)
				return false;
		} else if (!products.equals(other.products))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProductDTOPost [id=" + id + ", name=" + name + ", description=" + description + ", images=" + images
				+ ", products=" + products + "]";
	}
}
