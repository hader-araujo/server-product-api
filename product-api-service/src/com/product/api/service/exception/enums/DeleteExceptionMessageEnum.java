package com.product.api.service.exception.enums;

public enum DeleteExceptionMessageEnum {
	UNEXPECTED_EXCEPTION, ID_NULL_EXCEPTION, NOT_FOUND
}
